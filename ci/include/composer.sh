#!/bin/sh

set -e

apk update --quiet
apk add --quiet --no-cache git

curl https://composer.github.io/installer.sig -s -S | tr -d '\n' > installer.sig
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
php -r "if (hash_file('SHA384', 'composer-setup.php') === file_get_contents('installer.sig')) { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
php composer-setup.php
php -r "unlink('composer-setup.php'); unlink('installer.sig');"
mv composer.phar /usr/local/bin/composer

composer install --no-progress --no-interaction --no-suggest
