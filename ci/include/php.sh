#!/bin/sh

set -e

apk update --quiet
apk add --quiet --no-cache icu-dev

docker-php-ext-install intl