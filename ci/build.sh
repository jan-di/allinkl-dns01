#!/bin/sh

set -xe

sh ci/include/php.sh
sh ci/include/composer.sh
sh ci/include/phive.sh

tools/box compile
cp -R example/manual-hook out/manual-hook/