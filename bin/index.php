<?php

use JanDi\AllInklDns01\Command\AuthCommand;
use JanDi\AllInklDns01\Command\CleanupCommand;
use Symfony\Component\Console\Application;

require __DIR__.'/../vendor/autoload.php';

$application = new Application();

$application->add(new AuthCommand());
$application->add(new CleanupCommand());

$application->run();
