# AllInkl DNS-01 Challenge Hooks #

## Installation ##

Make sure you have the following applications installed:
  - PHP v7.0+ with the Soap extension
  - [Certbot](https://certbot.eff.org/)

```shell
# Install current release
wget <url-to-latest-release>

# Unzip download
mkdir allinkl-dns01
tar xvzf allinkl-dns01.tar.gz -C allinkl-dns01

# Copy binary to a folder on $PATH
cp allinkl-dns01/allinkl-dns01.phar /usr/local/bin/allinkl-dns01

# Copy example hook scripts  and make them executable
cp -r allinkl-dns01/manual-hook /etc/letsencrypt/manual-hook/
chmod +x /etc/letsencrypt/manual-hook/allinkl-dns01-*.sh

# Cleanup
rm -r allinkl-dns01
rm allinkl-dns01.tar.gz
```

## Usage ##

1. Open the `/etc/letsencrypt/manual-hooks/.env` file and fill in your KAS credentials
```shell
export KASAPI_USERNAME='w012345a'
export KASAPI_PASSWORD='secretpassword'
```

2. Create your initial Certificate
```shell
certbot certonly --manual --preferred-challenges=dns --manual-auth-hook /etc/letsencrypt/manual-hook/allinkl-dns01-auth.sh --manual-cleanup-hook /etc/letsencrypt/manual-hook/allinkl-dns01-cleanup.sh --manual-public-ip-logging-ok --agree-tos -d <domain>
```

3. Test the renewal process with `certbot renew --dry-run`
4. Setup a cronjob to automate the certificate renewal with `certbot renew` (if not automatically done by certbot)
