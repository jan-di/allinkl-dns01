<?php

namespace JanDi\AllInklDns01\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use KasApi\KasConfiguration;
use KasApi\KasApi;

class AuthCommand extends Command
{
    protected static $defaultName = 'auth';

    protected function configure()
    {
        $this->setDescription('Auth Hook');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);

        $io->title("# Auth Hook #");

        $certbotDomain = getenv('CERTBOT_DOMAIN');
        $certbotSecret = getenv('CERTBOT_VALIDATION');
        $kasUsername = getenv('KASAPI_USERNAME');
        $kasPassword = getenv('KASAPI_PASSWORD');

        $io->text("KAS-Login: $kasUsername");
        $io->text("Domain: $certbotDomain");
        $io->newLine();
        
        $kasConfiguration = new KasConfiguration($kasUsername, sha1($kasPassword), 'sha1');
        $kasApi = new KasApi($kasConfiguration);

        $io->text('Creating a new dns record..');
        sleep(2);
        $recordId = $kasApi->add_dns_settings([
            'zone_host' => "$certbotDomain.",
            'record_name' => '_acme-challenge',
            'record_type' => 'TXT',
            'record_data' => $certbotSecret,
            'record_aux' => 0
        ]);

        $io->success("Successfully created entry #$recordId for domain '$certbotDomain'.");
    }
}
