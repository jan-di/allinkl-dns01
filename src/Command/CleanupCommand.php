<?php

namespace JanDi\AllInklDns01\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use KasApi\KasConfiguration;
use KasApi\KasApi;

class CleanupCommand extends Command
{
    protected static $defaultName = 'cleanup';

    protected function configure()
    {
        $this->setDescription('Cleanup Hook');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);

        $io->title("# Cleanup Hook #");

        $certbotDomain = getenv('CERTBOT_DOMAIN');
        $certbotSecret = getenv('CERTBOT_VALIDATION');
        $kasUsername = getenv('KASAPI_USERNAME');
        $kasPassword = getenv('KASAPI_PASSWORD');

        $io->text("KAS-Login: $kasUsername");
        $io->text("Domain: $certbotDomain");
        $io->newLine();

        $kasConfiguration = new KasConfiguration($kasUsername, sha1($kasPassword), 'sha1');
        $kasApi = new KasApi($kasConfiguration);

        $io->text("Check if an acme-challenge record exists..");
        $dnsSettings = $kasApi->get_dns_settings(['zone_host' => "$certbotDomain."]);
        sleep(2);
        $recordId = null;
        foreach ($dnsSettings as $setting) {
            if ($setting['record_type'] == 'TXT' && $setting['record_name'] == '_acme-challenge') {
                $recordId = $setting['record_id'];
                break;
            }
        }

        if ($recordId !== null) {
            $io->text("Record found. Deleting..");
            sleep(2);
            $kasApi->delete_dns_settings(['record_id' => $recordId]);

            $io->success("Successfully deleted entry #$recordId for domain '$certbotDomain'.");
        } else {
            $io->success("There is no record to delete");
        }
    }
}
